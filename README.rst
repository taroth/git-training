==============
 Git Training
==============

This is the source text for a git training document.

The html for this document is automatically generated and viewable at:

    http://openavr.gitlab.io/git-training/

Setting up to build the Document
================================

The Document is generated using the Sphinx_ tool from |RST|_ source files.

To setup a build environment, run the `setup` script::

    $ cd ${WORKDIR}
    $ git clone https://gitlab.com/OpenAVR/git-training.git
    $ cd git-training
    $ ./setup

This will create a python virtualenv and install sphinx into it. You will need
to activate the virtual environment before it can be used to build the
document::

    $ source tool-env/bin/activate

To generate the ``html`` version of the document::

    $ make html

To generate the ``pdf`` version of the document::

    $ make latexpdf

You will likely need to install ``LaTeX`` packages to build the ``pdf`` version
of the document (left as an exercise for the reader).

When you are done generating the document, you probably want to deactivate the
virtual environment by logging out of the shell or by running the following::

    $ deactivate

.. _Sphinx: http://www.sphinx-doc.org/

.. |RST| replace:: reStructuredText
.. _RST: http://docutils.sourceforge.net/rst.html

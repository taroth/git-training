Git and SVN
===========

It is possible to use :command:`git` as a front end to subversion
repositories using `git-svn(1)
<http://www.kernel.org/pub/software/scm/git/docs/git-svn.html>`_.

The :command:`git-svn` is also very useful when you need to convert an SVN
repository into a git repository with little to no loss of history.

Creating a Git Repo from an SVN Repo
------------------------------------

.. todo:: Add discussion of :command:`git-svn`

See: https://git-scm.com/book/en/v2/Git-and-Other-Systems-Git-as-a-Client

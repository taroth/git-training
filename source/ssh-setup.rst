.. _ssh-setup-label:

SSH Setup
=========

In order to access a git service using git, you will need to create an ssh key
pair and install it on the git service.

The following examples will use |gitlab| as an example. Other services may be
slightly different in some respects, but the overall theory should be similar.

Generating a SSH key
--------------------

If you have not created a ssh key, use the following command to
generate one (do this from your workstation so you will have the key
available for use after the training)::

    $ cd ~/.ssh
    $ ssh-keygen -t ed25519 -f id_ed25519_gitlab

Some git hosting services may not yet support ``ed25519`` keys. If the service
you want to use does not support ``ed25519`` keys, generate your key pair with
the following::

    $ ssh-keygen -t rsa -b 4096 -f id_rsa_gitlab

Next, will you need to upload the public key (:file:`id_ed25519_git.pub`) to
the git repository hosting service.

.. note::

   It is not required to name your key :file:`id_ed25519_git`. Doing so only
   makes it clear to you what the key is used for. Any ssh key that you have
   already created can be used if you do not want to create another key. The
   only requirement is that a public key identifying you is installed on the
   hosting service's server.

Upload Public Key to |gitlab|
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Login to |gitlab| and install the contents of the public key file using this
page:

    https://gitlab.com/profile/keys

Test the key installation with (you should get you user name in the response
instead of mine)::

    $ ssh -i ~/.ssh/id_ed25519_git git@gitlab.com
    Enter passphrase for key '/home/troth/.ssh/id_ed25519_git':
    PTY allocation request failed on channel 0
    Welcome to GitLab, Theodore A. Roth!
    Connection to gitlab.com closed.

Upload Public Key to |github|
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Login to |github| and install the contents of the public key file using this
page:

    https://github.com/settings/keys

Test the key installation with::

    $ ssh -i ~/.ssh/id_ed25519_git git@github.com
    Enter passphrase for key '/home/troth/.ssh/id_ed25519_git':
    PTY allocation request failed on channel 0
    Hi troth! You've successfully authenticated, but GitHub does not provide shell access.
    Connection to github.com closed.

Upload Public Key to |bitbucket|
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Login to |bitbucket| and install the contents of the public key file using this
page:

    https://bitbucket.org/account/user/your-user-id/ssh-keys/

Test the key installation with::

    $ ssh -i ~/.ssh/id_ed25519_git git@bitbucket.org
    Enter passphrase for key '/home/troth/.ssh/id_ed25519_git':
    PTY allocation request failed on channel 0
    logged in as taroth.

    You can use git or hg to connect to Bitbucket. Shell access is disabled.
    Connection to bitbucket.org closed.

Adding an SSH Config Entry
--------------------------

You can make your life a little easier by adding an entry for the hosting
service to your :file:`~/.ssh/config` file.

Add the following to your :file:`~/.ssh/config` file on your
workstation::

    $ cat >>~/.ssh/config <<EOF
    Host gitlab
         User git
         Hostname gitlab.com
         Port 22
         Identityfile ~/.ssh/id_ed25519_git
    EOF

Add similar entries for |github| or |bitbucket| should you wish to use those
services.

With this configuration entry in place, you can now just use the following to
test access to the service::

    $ ssh gitlab
    Enter passphrase for key '/home/troth/.ssh/keys/id_ed25519_gitlab':
    PTY allocation request failed on channel 0
    Welcome to GitLab, Theodore A. Roth!
    Connection to gitlab.com closed.

Tips and Tricks
===============

Git can do a lot more things than we have seen during this training.
This section will cover some of the more interesting things you can do
with git.

git grep
--------

The :command:`git grep` command is useful for searching through all of
the files that are under revision control. Files that are not under
revision control are ignored.

git add -i
----------

The :command:`git add -i` command makes the :command:`git add` command
interactive. You can select which files to add, update or patch. It is
quite powerful and well worth learning how to use.

* https://git-scm.com/book/en/v2/Git-Tools-Interactive-Staging

I also find :command:`git add -p` to be quite useful.

For more information::

    $ git help add

gitk
----

The :command:`gitk` tool provides a graphic display of the history of
the repository. To use :command:`gitk`, you may need to install the
:command:`gitk` package (using apt-get, yum or whatever your system
uses to manage packages).

Additional Resources
====================

After finishing this training, you will likely be interested in some
additional resources related to Git.

Git Web Site
------------

The Git Web Site has loads of information:

    http://git-scm.com/

Pro Git Book
------------

The Pro Git book is freely available online:

    https://git-scm.com/book

Git Repository Hosting Services
-------------------------------

There are many cloud based services for git repositories. The following are the
big three. There are many more features of each service than provided here, do
your own research to see which suits your needs best.

|gitlab|_
^^^^^^^^^

https://gitlab.com/

* Free public and private repositories.
* GitLab Pages can be used to host docs: https://about.gitlab.com/features/pages/

|github|_
^^^^^^^^^

https://github.com/

* Free for public repositories.
* Must pay for private repositories.
* GitHub Pages are a great way to host websites.

|bitbucket|_
^^^^^^^^^^^^

https://bitbucket.org/

* Free public and private repositories.
* Need to pay if you need to give more than five people access to a private
  repo.
* Great Jira integration since Atlassian runs both services.

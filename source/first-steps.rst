First Steps
===========

There are a few book keeping things you should do before using git.

Set Your Identity
-----------------

Git needs to know your name and email address which are used in commit
messages. Use the following commands to set up your identity::

  [1]$ git config --global user.name "John Doe"
  [2]$ git config --global user.email johndoe@hp.com

These commands will modify your :file:`~/.gitconfig` file which is your global
config information.

You can override global config values in each repo by editing the
:file:`<repo>/.git/config` file.

Getting Help
------------

All git commands are well documented via git's online documentation::

  [3]$ git help <verb>       # Full text of help (shows the man page version)
  [4]$ git <verb> --help     # Usually equivalent to 'git help <verb>'
  [5]$ git <verb> -h         # Usually less verbose, just listing options
  [6]$ man git-<verb>

Try the following to get more information about git configuration::

  [7]$ git help config

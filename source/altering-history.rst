Altering History
================

Git provides some tools and commands which allow you to alter the
history of your repository. Sometimes it's good to be able to change
history, but it can be dangerous.

It all boils down to one simple rule:

.. code-block:: none

  Once you have shared your changes with another repository (by either
  you pushing, or someone else pulling), those changes should not be
  changed. Doing so will cause all kinds of grief for the user of the
  other repository.

.. note::
   Modern version of |git| are much more forgiving when you pull changes from a
   remote branch which has altered its history. Still, make sure you understand
   the consequences of pushing altered history.

The following are some commands that change history. This discussion
is only provided to make you aware of the existence of these features,
not to be a comprehensive overview. If you need to use these commands,
you should do some more research into them to gain a better
understanding of the pros and cons of the commands.

git rebase
----------

The :command:`git rebase` command is used to alter where a sequence of
commits is based.

For example, you created a branch off master, then you make a series
of changes. While you were making your changes, master has
evolved. Rebasing your branch to the current master will make all of
your changes relative to the last change on master.

By comparison, doing a merge of master onto your branch would place
all of the changes since you branched on top of your changes.

git rebase -i
-------------

Interactive rebasing allows you to change alter the history on a commit by
commit basis. Some of the things you can do with interactive rebasing include:

* Change the ordering of commits.
* Alter commit messages.
* Modify a commit.

git commit --amend
----------------------

The :command:`git commit --amend` command allows you to squash your
current change into the last committed change.

For example, you just committed a change and then you realize that you
introduced a typo. Using :command:`git commit --amend` allows you to
fix the typo and the history will never even show that the typo existed.

git reset
---------

The :command:`git reset` command adjusts the HEAD ref to a given
commit.

For example, you just committed a change and you realize that you
really didn't want to make that change. You could revert it, but that
would leave history of the bad change. Using :command:`git reset` you
can rewind history so that HEAD points to the previous commit (or any
prior commit you want).
